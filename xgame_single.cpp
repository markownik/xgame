#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <string>
#include <ctime>

using namespace std;

string intToStr(int n)
{
	string tmp;
	if(n < 0) {
		tmp = "-";
		n = -n;
	}
	if(n > 9)
		tmp += intToStr(n / 10);
	tmp += n % 10 + 48;
	return tmp;
}

int randBetween(int min, int max){
	return rand()%(max-min + 1) + min;
}

class WindowManager
{
private:
	Display * display;
	int screen;
	Window window;
	GC context;
	XFontStruct * font;
	int points;
	int trials;
	int padding;
	int padding_hold;
	int last_key_press = -1;
	int hit = 0;
	
	GC green_gc;
	XColor green_col;
	string green = "#00FF00";
	
	GC red_gc;
	XColor red_col;
	string red = "#FF0000";
	
	Colormap colormap;
	
public:
	int width;
	int height;
	XWindowAttributes attr;
	unsigned long black_pixel;
	unsigned long white_pixel;
	
	void check_window(int width, int height)
	{
		if(width < 300)
		{
			width = 300;
			XResizeWindow(display, window, width, height);
		}
		if(height < 200)
		{
			height = 200;
			XResizeWindow(display, window, width, height);
		}
	}
	
	int shot()
	{
		return randBetween(1,4);
	}
	
	int getPart(int x, int y)
	{
		if (x < width/2 && x > padding && y > padding && y < height/2){
			return 1;
		}
		else if (x > width/2 && x < width - padding && y > padding && y < height/2){
			return 2;
		}
		else if (x < width/2 && x > padding && y > height/2 && y < height - padding){
			return 3;
		}
		else if (x > width/2 && x < width - padding && y > height/2 && y < height - padding){
			return 4;
		}
		else
			return 0;
	}
	
	void init(int w, int h, string player2)
	{
		if (player2 != "")
		{
			display = XOpenDisplay(player2.c_str());
			//display = XOpenDisplay( NULL );
		}
		else
		{
			display = XOpenDisplay( NULL );
		}
		
		screen = DefaultScreen(display);
		black_pixel = BlackPixel(display, screen);
		white_pixel = WhitePixel(display, screen);
		width = w; height = h;
		window = XCreateSimpleWindow( display, RootWindow(display, screen), 0, 0, width, height, 0, black_pixel, black_pixel );
		XGetWindowAttributes(display, window, &attr);
		
		Atom delWindow = XInternAtom( display, "WM_DELETE_WINDOW", 0 );
		XSetWMProtocols(display, window, &delWindow, 1);
		
		XSelectInput(display, window, ExposureMask | PointerMotionMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask);
		
		XMapWindow(display, window);
		
		context = XCreateGC(display, window, 0, NULL );
		
		XSetForeground(display, context, white_pixel);
		
		XSizeHints* win_size_hints = XAllocSizeHints();
		if (!win_size_hints) {
			fprintf(stderr, "XAllocSizeHints - out of memory\n");
			exit(1);
		}
		win_size_hints->flags = PSize | PMinSize;
		win_size_hints->min_width = 300;
		win_size_hints->min_height = 200;
		win_size_hints->base_width = width;
		win_size_hints->base_height = height;
		XSetWMNormalHints(display, window, win_size_hints);
		XFree(win_size_hints);
		
		
		colormap = DefaultColormap(display, 0);
		
		green_gc = XCreateGC(display, window, 0, 0);
		XParseColor(display, colormap, green.c_str(), &green_col);
		XAllocColor(display, colormap, &green_col);
		
		red_gc = XCreateGC(display, window, 0, 0);
		XParseColor(display, colormap, red.c_str(), &red_col);
		XAllocColor(display, colormap, &red_col);
		
		
		
		
		points = 0;
		trials = 0;
		padding = 30;
		padding_hold = padding;
		
	}
	
	Display* getDisplay()
	{
		return display;
	}
	
	void close()
	{
		XFreeGC( display, green_gc );
		XFreeGC( display, red_gc );
		XFreeGC( display, context );
		XDestroyWindow( display, window );
		XCloseDisplay( display );
	}
	
	void draw()
	{
		string buff;
		XClearWindow (display, window);
		if(padding > 0)
			XDrawRectangle(display, window, context, padding, padding, width-2*padding, height-2*padding);
		XDrawLine( display, window, context, width/2, 0+padding, width/2, height-padding );
		XDrawLine( display, window, context, 0+padding, height/2, width-padding, height/2 );
		buff = "Points: ";
		buff += intToStr(points);
		XDrawImageString(display, window, context, width/10, 13, buff.c_str(), buff.length());
		buff = "Tries: ";
		buff += intToStr(trials);
		XDrawImageString(display, window, context, 3*width/10, 13, buff.c_str(), buff.length());
		buff = "1";
		XDrawImageString(display, window, context, padding + 5, padding + 13, buff.c_str(), buff.length());
		buff = "2";
		XDrawImageString(display, window, context, width/2 + 5, padding + 13, buff.c_str(), buff.length());
		buff = "3";
		XDrawImageString(display, window, context, padding + 5, height/2 + 13, buff.c_str(), buff.length());
		buff = "4";
		XDrawImageString(display, window, context, width/2 + 5, height/2 + 13, buff.c_str(), buff.length());
		XFlush( display );
		
	}
	
	void draw_hit(){
		string buff;
		
		if(hit < 0){
			XSetForeground(display, context, red_col.pixel);
			XSetBackground(display, context, red_col.pixel);
		}
		
		if(hit > 0){
			XSetForeground(display, context, green_col.pixel);
			XSetBackground(display, context, green_col.pixel);
		}
		
		switch(abs(hit)){
			case 1:
				XFillRectangle(display, window, context, padding+1, padding+1, width/2-padding-1, height/2-padding-1);
				XSetForeground(display, context, black_pixel);
				buff = "1";
				XDrawImageString(display, window, context, padding + 5, padding + 13, buff.c_str(), buff.length());
				break;
			case 2:
				XFillRectangle(display, window, context, width/2+1, padding+1, width/2-padding-1, height/2-padding-1);
				XSetForeground(display, context, black_pixel);
				buff = "2";
				XDrawImageString(display, window, context, width/2 + 5, padding + 13, buff.c_str(), buff.length());
				break;
			case 3:
				XFillRectangle(display, window, context, padding+1, height/2+1, width/2-padding-1, height/2-padding-1);
				XSetForeground(display, context, black_pixel);
				buff = "3";
				XDrawImageString(display, window, context, padding + 5, height/2 + 13, buff.c_str(), buff.length());
				break;
			case 4:
				XFillRectangle(display, window, context, width/2+1, height/2+1, width/2-padding-1, height/2-padding-1);
				XSetForeground(display, context, black_pixel);
				buff = "4";
				XDrawImageString(display, window, context, width/2 + 5, height/2 + 13, buff.c_str(), buff.length());
				break;
		}
		
		if(hit != 0)
		{
			XSetForeground(display, context, white_pixel);
			XSetBackground(display, context, black_pixel);
			hit = 0;
		}
	}
	
	void run()
	{
		short done = 0;
		int current_target;
		while(!done) {
			
			XEvent evt;
			current_target = 0;
			XNextEvent(display, &evt);
			string buff;
			
			switch( evt.type ){
				case KeyPress : {
					if (evt.xkey.keycode != last_key_press){
						current_target = shot();
						switch (evt.xkey.keycode) {
							case 26:
								trials++;
								if(current_target==1){
									points++;
									hit = 1;
								}else{
									hit = -1;
								}
								break;
								
							case 27:
								trials++;
								if(current_target==2){
									points++;
									hit = 2;
								}else{
									hit = -2;
								}
								break;
								
							case 28:
								trials++;
								if(current_target==3){
									points++;
									hit = 3;
								}else{
									hit = -3;
								}
								break;
								
							case 29:
								trials++;
								if(current_target==4){
									points++;
									hit = 4;
								}else{
									hit = -4;
								}
								break;
								
							case 61:
								
								done = 1;
								break;
						}
						
						draw_hit();
						
						buff = "KeyPressCode: ";
						buff += intToStr(evt.xkey.keycode);
						cout << buff << endl;
						buff = "Points: ";
						buff += intToStr(points);
						XDrawImageString(display, window, context, width/10, 13, buff.c_str(), buff.length());
						buff = "Tries: ";
						buff += intToStr(trials);
						XDrawImageString(display, window, context, 3*width/10, 13, buff.c_str(), buff.length());
						last_key_press = evt.xkey.keycode;
						//last_key_released = -1;
					}
					break;
					
				}
					
					
				case KeyRelease : {
					
					if (evt.xkey.keycode == last_key_press) {
						
						draw();
						buff = "KeyReleaseCode: ";
						buff += intToStr(evt.xkey.keycode);
						cout << buff << endl;
						
						last_key_press = -1;
					}
					break;
				}
					
				case ButtonPress : {
					current_target = shot();
					switch( evt.xbutton.button ){
						case Button1 : {
							int player_part = getPart(evt.xbutton.x, evt.xbutton.y);
							trials++;
							hit = player_part;
							if(current_target==player_part)
								points++;
							else
								hit = -player_part;
							
							buff = "Clicked: ";
							buff += intToStr(player_part);
							cout << buff << endl;
							
							draw_hit();
							break;
						}
					}
					buff = "Points: ";
					buff += intToStr(points);
					XDrawImageString(display, window, context, width/10, 13, buff.c_str(), buff.length());
					buff = "Tries: ";
					buff += intToStr(trials);
					XDrawImageString(display, window, context, 3*width/10, 13, buff.c_str(), buff.length());
					break;
				}
					
				case ButtonRelease : {
					draw();
					buff = "ButtonReleased";
					cout << buff << endl;
					break;
				}
					
				case ClientMessage : {
					done = 1;
					break;
				}
					
				case Expose : {
					XGetWindowAttributes(display, window, &attr);
					width = attr.width; height = attr.height;
					if ((width < 400 || height < 300) && padding != 0){padding = 0;}
					if ((width >= 400 || height >= 300) && padding != padding_hold){padding = padding_hold;}
					if( evt.xexpose.count == 0 )
					{
						draw();
					}
					break;
				}
			}
			
		}
	}
};


int main(int argc, char *argv[])
{
	srand(static_cast <unsigned int> (time(NULL)));
	WindowManager mywindow;
	//for (int i = 0; i < argc; i++)
	//{
	//cout << argv[i] << endl;
	//}
	if(argc>=2){
		string ip_addr = argv[1];
		cout << ip_addr << endl;
		//mywindow.init(600,400,);
		mywindow.init(600,400,ip_addr);
	}else{
		mywindow.init(600,400,"");
	}
	mywindow.run();
	mywindow.close();
	return 0;
}