all: xgame.o link

xgame.o: xgame.cpp
	g++ -c xgame.cpp -I/usr/X11R6/include -std=c++11

link: xgame.o
#	g++ xgame.o -L/usr/X11R6/lib64 -lX11 -o xgame
	g++ xgame.o -L/usr/X11R6/lib -lX11 -o xgame 

clean: 
	rm xgame.o xgame 

