#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <string>

using namespace std;

string intToStr(int n)
{
	string tmp;
	if(n < 0) {
		tmp = "-";
		n = -n;
	}
	if(n > 9)
		tmp += intToStr(n / 10);
	tmp += n % 10 + 48;
	return tmp;
}

int randBetween(int min, int max){
	return rand()%(max-min + 1) + min;
}

class Player
{
public:
	string name = "Player";
	int id = 0;
	Display * display;
	int screen;
	Window window;
	GC context;
	int points = 0;
	int trials = 0;
	int last_key_press = -1;
	int hit = 0;
	int width;
	int height;
	XWindowAttributes attr;
	unsigned long black_pixel;
	unsigned long white_pixel;
	int padding;
	int padding_hold;
	XFontStruct * font;
	
	GC green_gc;
	XColor green_col;
	string green = "#00FF00";
	
	GC red_gc;
	XColor red_col;
	string red = "#FF0000";
	
	GC yellow_gc;
	XColor yellow_col;
	string yellow = "#FFFF00";
	
	Colormap colormap;
	
	int getPart(int x, int y)
	{
		if (x < width/2 && x > padding && y > padding && y < height/2){
			return 1;
		}
		else if (x > width/2 && x < width - padding && y > padding && y < height/2){
			return 2;
		}
		else if (x < width/2 && x > padding && y > height/2 && y < height - padding){
			return 3;
		}
		else if (x > width/2 && x < width - padding && y > height/2 && y < height - padding){
			return 4;
		}
		else
			return 0;
	}
	
	void init(int w, int h, int id_, string ip_addr)
	{
		if(ip_addr == "")
			display = XOpenDisplay(NULL);
		else
			display = XOpenDisplay(ip_addr.c_str());
		id = id_;
		name = name + intToStr(id);
		screen = DefaultScreen(display);
		black_pixel = BlackPixel(display, screen);
		white_pixel = WhitePixel(display, screen);
		width = w; height = h;
		window = XCreateSimpleWindow( display, RootWindow(display, screen), 0, 0, width, height, 0, black_pixel, black_pixel );
		XGetWindowAttributes(display, window, &attr);
		
		Atom delWindow = XInternAtom( display, "WM_DELETE_WINDOW", 0 );
		XSetWMProtocols(display, window, &delWindow, 1);
		
		XSelectInput(display, window, ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask);
		
		XMapWindow(display, window);
		
		context = XCreateGC(display, window, 0, NULL );
		
		XSetForeground(display, context, white_pixel);
		
		XSizeHints* win_size_hints = XAllocSizeHints();
		if (!win_size_hints) {
			fprintf(stderr, "XAllocSizeHints - out of memory\n");
			exit(1);
		}
		win_size_hints->flags = PSize | PMinSize | PMaxSize;
		win_size_hints->min_width = width;
		win_size_hints->min_height = height;
		win_size_hints->max_width = width;
		win_size_hints->max_height = height;
		win_size_hints->base_width = width;
		win_size_hints->base_height = height;
		XSetWMNormalHints(display, window, win_size_hints);
		XFree(win_size_hints);
		
		colormap = DefaultColormap(display, 0);
		
		green_gc = XCreateGC(display, window, 0, 0);
		XParseColor(display, colormap, green.c_str(), &green_col);
		XAllocColor(display, colormap, &green_col);
		
		red_gc = XCreateGC(display, window, 0, 0);
		XParseColor(display, colormap, red.c_str(), &red_col);
		XAllocColor(display, colormap, &red_col);
		
		yellow_gc = XCreateGC(display, window, 0, 0);
		XParseColor(display, colormap, yellow.c_str(), &yellow_col);
		XAllocColor(display, colormap, &yellow_col);
		
		points = 0;
		trials = 0;
		padding = 30;
		padding_hold = padding;
	}
	
	void close()
	{
		XFreeGC( display, green_gc );
		XFreeGC( display, red_gc );
		XFreeGC( display, context );
		XDestroyWindow( display, window );
		XCloseDisplay( display );
	}
	
	void draw_hit(int selected = 0) {
		string buff;
		
		if(hit < 0){
			XSetForeground(display, context, red_col.pixel);
			XSetBackground(display, context, red_col.pixel);
		}
		
		if(hit > 0){
			XSetForeground(display, context, green_col.pixel);
			XSetBackground(display, context, green_col.pixel);
		}
		
		if(selected == 1)
		{
			XSetForeground(display, context, yellow_col.pixel);
			XSetBackground(display, context, yellow_col.pixel);
		}
		
		switch(abs(hit)){
			case 1:
				XFillRectangle(display, window, context, padding+1, padding+1, width/2-padding-1, height/2-padding-1);
				XSetForeground(display, context, black_pixel);
				buff = "1";
				XDrawImageString(display, window, context, padding + 5, padding + 13, buff.c_str(), buff.length());
				break;
			case 2:
				XFillRectangle(display, window, context, width/2+1, padding+1, width/2-padding-1, height/2-padding-1);
				XSetForeground(display, context, black_pixel);
				buff = "2";
				XDrawImageString(display, window, context, width/2 + 5, padding + 13, buff.c_str(), buff.length());
				break;
			case 3:
				XFillRectangle(display, window, context, padding+1, height/2+1, width/2-padding-1, height/2-padding-1);
				XSetForeground(display, context, black_pixel);
				buff = "3";
				XDrawImageString(display, window, context, padding + 5, height/2 + 13, buff.c_str(), buff.length());
				break;
			case 4:
				XFillRectangle(display, window, context, width/2+1, height/2+1, width/2-padding-1, height/2-padding-1);
				XSetForeground(display, context, black_pixel);
				buff = "4";
				XDrawImageString(display, window, context, width/2 + 5, height/2 + 13, buff.c_str(), buff.length());
				break;
		}
		
		
		if(hit != 0)
		{
			XSetForeground(display, context, white_pixel);
			XSetBackground(display, context, black_pixel);
			hit = 0;
		}
	}
	
	void draw() {
		XGetWindowAttributes(display, window, &attr);
		width = attr.width; height = attr.height;
		string buff;
		XClearWindow (display, window);
		if(padding > 0)
			XDrawRectangle(display, window, context, padding, padding, width-2*padding, height-2*padding);
		XDrawLine( display, window, context, width/2, 0+padding, width/2, height-padding );
		XDrawLine( display, window, context, 0+padding, height/2, width-padding, height/2 );
		buff = name+" Points: ";
		buff += intToStr(points);
		switch(id){
			case 1:
				XDrawImageString(display, window, context, width/10, 13, buff.c_str(), buff.length());
				break;
			case 2:
				XDrawImageString(display, window, context, 6*width/10, 13, buff.c_str(), buff.length());
				break;
		}
		buff = name+" Tries: ";
		buff += intToStr(trials);
		switch(id){
			case 1:
				XDrawImageString(display, window, context, 3*width/10, 13, buff.c_str(), buff.length());
				break;
			case 2:
				XDrawImageString(display, window, context, 8*width/10, 13, buff.c_str(), buff.length());
				break;
		}
		buff = "1";
		XDrawImageString(display, window, context, padding + 5, padding + 13, buff.c_str(), buff.length());
		buff = "2";
		XDrawImageString(display, window, context, width/2 + 5, padding + 13, buff.c_str(), buff.length());
		buff = "3";
		XDrawImageString(display, window, context, padding + 5, height/2 + 13, buff.c_str(), buff.length());
		buff = "4";
		XDrawImageString(display, window, context, width/2 + 5, height/2 + 13, buff.c_str(), buff.length());
		XFlush( display );
	}
	
	
};

class GameManager
{
private:
	Player player1;
	Player player2;
	int padding = 30;
	bool multiplayer = false;
	int gamestate = 0;
	int counter = 0;
	int current_target = 0;
	
public:
	
	int init(int w, int h, string player2_display)
	{
		Display *test;
		test = XOpenDisplay(player2_display.c_str());
		
		if (!test){
			multiplayer = false;
			cout << "Opening another display '" << player2_display << "' failed. Single Player Mode not yet supported :(" << endl;
			return 1;
		}else{
			XCloseDisplay(test);
			player2.init(w,h,2,player2_display.c_str());
			player1.init(w,h,1,"");
			multiplayer = true;
			player1.padding = padding;
			player1.padding_hold = padding;
			player2.padding = padding;
			player2.padding_hold = padding;
			return 0;
		}
	}
	
	void close()
	{
		player1.close();
		player2.close();
	}
	
	void draw_commands()
	{
		switch(gamestate)
		{
			case 0:
				XDrawString(player1.display, player1.window, player1.context, player1.padding, player1.height-player1.padding/2, "Choose one field!", 17);
				break;
			case 1:
				XDrawString(player2.display, player2.window, player2.context, player2.padding, player2.height-player2.padding/2, "Trick the other player, and take a lucky guess!", 47);
				break;
			case 2:
				XDrawString(player2.display, player2.window, player2.context, player2.padding, player2.height-player2.padding/2, "Choose one field!", 17);
				break;
			case 3:
				XDrawString(player1.display, player1.window, player1.context, player1.padding, player1.height-player1.padding/2, "Trick the other player, and take a lucky guess!", 47);
				break;
		}
	}
	
	void draw()
	{
		gamestate=counter%4;
		player1.draw();
		player2.draw();
		draw_commands();
	}
	
	void choice(Player player)
	{
		counter++;
		cout << player.name << " have chosen tile #" << current_target << endl;
		draw();
		player.hit = current_target;
		player.draw_hit(1);
	}
	
	int shot(Player player, int sht)
	{
		int win = 0;
		counter++;
		cout << player.name << " took a shot on tile #" << sht << endl;
		player.hit = -sht;
		draw();
		player.draw_hit();
		if(current_target==sht)
		{
			win = 1;
			player.hit = current_target;
		}
		player.draw_hit();
		if(current_target != sht)
		{
			player.hit = current_target;
			player.draw_hit();
		}
		current_target = 0;
		return win;
	}
	
	void run()
	{
		short done = 0;
		while(!done) {
			
			XEvent evt;
			XEvent evt2;
			gamestate=counter%4;
			
			while(XPending(player1.display))
			{
				cout << counter << " -> gamestate: " << gamestate << endl;
				XNextEvent(player1.display, &evt);
				switch(evt.type)
				{
						
					case ButtonPress :
						if(gamestate == 0 || gamestate == 3){
							switch( evt.xbutton.button ){
								case Button1 :
									switch(gamestate){
										case 0:
											current_target = player1.getPart(evt.xbutton.x, evt.xbutton.y);
											if(current_target!=0){
												choice(player1);
											}
											break;
											
										case 3:
											
											int bum = player1.getPart(evt.xbutton.x, evt.xbutton.y);
											if(bum!=0){
												player1.trials++;
												player1.points += shot(player1, bum);
												cout << player1.name << " score: " << player1.points << endl << player2.name << " score: " << player2.points << endl;
											}
											break;
									}
									break;
							}
						}
						break;
						
					case Expose:
						if( evt.xexpose.count == 0 )
						{
							player1.draw();
							draw_commands();
						}
						break;
						
					case KeyPress:
						if(gamestate == 0 || gamestate == 3){
							int bum = 0;
							switch (evt.xkey.keycode) {
								case 26: //1
									current_target = 1;
									bum = 1;
									break;
									
								case 27: //2
									current_target = 2;
									bum = 2;
									break;
									
								case 28: //3
									current_target = 3;
									bum = 3;
									break;
									
								case 29: //4
									current_target = 4;
									bum = 4;
									break;
									
								case 61: //esc
									done = 1;
									break;
							}
							switch(gamestate){
								case 0:
									if(current_target!=0){
										choice(player1);
									}
									break;
									
								case 3:
									if(bum!=0){
										player1.trials++;
										player1.points += shot(player1, bum);
										cout << player1.name << " score: " << player1.points << endl << player2.name << " score: " << player2.points << endl;
									}
									break;
							}
						}
						break;
					case KeyRelease :
						break;
					case ButtonRelease :
						break;
						
				}
			}
			
			
			while(XPending(player2.display))
			{
				cout << counter << " -> gamestate: " << gamestate << endl;
				XNextEvent( player2.display, &evt2 );
				switch( evt2.type ){
					case ButtonPress :
						if(gamestate == 1 || gamestate == 2){
							switch( evt2.xbutton.button ){
								case Button1 :
									switch(gamestate){
										case 2:
											current_target = player2.getPart(evt2.xbutton.x, evt2.xbutton.y);
											if(current_target!=0){
												choice(player2);
											}
											break;
											
										case 1:
											
											int bum = player2.getPart(evt2.xbutton.x, evt2.xbutton.y);
											if(bum!=0){
												player2.trials++;
												player2.points += shot(player2, bum);
												cout << player1.name << " score: " << player1.points << endl << player2.name << " score: " << player2.points << endl;
											}
											break;
									}
									break;
							}
						}
						break;
						
					case Expose:
						if( evt2.xexpose.count == 0 )
						{
							player2.draw();
							draw_commands();
						}
						break;
						
					case KeyPress:
						if(gamestate == 1 || gamestate == 2){
							int bum = 0;
							switch (evt2.xkey.keycode) {
								case 26: //1
									current_target = 1;
									bum = 1;
									break;
									
								case 27: //2
									current_target = 2;
									bum = 2;
									break;
									
								case 28: //3
									current_target = 3;
									bum = 3;
									break;
									
								case 29: //4
									current_target = 4;
									bum = 4;
									break;
									
								case 61: //esc
									done = 1;
									break;
							}
							switch(gamestate){
								case 2:
									if(current_target!=0){
										choice(player2);
									}
									break;
									
								case 1:
									if(bum!=0){
										player2.trials++;
										player2.points += shot(player2, bum);
										cout << player1.name << " score: " << player1.points << endl << player2.name << " score: " << player2.points << endl;
									}
									break;
							}
							break;
						}
					case KeyRelease :
						break;
					case ButtonRelease :
						break;
				}
				
			}
			
		}
	}
};


int main(int argc, char *argv[])
{
	srand(static_cast <unsigned int> (time(NULL)));
	GameManager myGame;
	string display_ip = "";
	if(argc>=2){
		display_ip = argv[1];
		cout << "Connecting to... " << display_ip << endl;
	}
	if(myGame.init(600,400,display_ip)==0){
		myGame.run();
		myGame.close();
	}else{
		cout << "Couldn't open given display!!! Exiting..." << endl;
	}
	return 0;
}